import os
import databricks.koalas as ks
from datetime import datetime as dtm
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import TimestampType, StringType
from pyspark.sql.functions import min, max, col, isnan, when, count
import logging


# Convert the timestamp to the date
def epoch_to_date(tstmp):
    dt = dtm.fromtimestamp(tstmp/1000)#.strftime('%Y-%m-%d')
    return dt


def date_to_ym(dt):
    return dt.strftime('%Y-%m-%d')


# Create the Spark Session
ss = SparkSession.builder.appName('Ocean Dewll').getOrCreate()
ss.sparkContext.setLogLevel('ERROR')

# Read all files from the data directory
data_dir = './data'
for idx, file in enumerate(os.listdir(data_dir)):
    if file.endswith('.parquet'):
        # Create the dataframe on the first file reading
        if idx == 0:
            df = ss.read.parquet(os.path.join(data_dir, file))
        else:
            df = df.union(ss.read.parquet(os.path.join(data_dir, file)))

print('Original dataset schema: ')
df.printSchema()
# Flattening the dataframe
tmpdf =  df.select(col('epochMillis'), col('mmsi'),
                   col('position.*'), col('navigation.*'),
                   col('olson_timezone'), col('vesselDetails.*'),
                   col('port.*'), col('imo'), col('callSign'),
                   col('destination'), col('cargoDetails'))

df = tmpdf.toDF('epochMillis','mmsi','latitude','longitude','navCode',
                'navDesc','courseOverGround','heading','rateOfTurn',
                'speedOverGround','olson_timezone','name','typeName',
                'typeCode','draught','length','width','flagCode',
                'flagCountry','unlocode','port_name','port_latitude',
                'port_longitude','imo','callSign','destination','cargoDetails')
# Print schema and count of rows
print('Flattened dataset schema: ')
df.printSchema()
print(f'Items in dataset: {df.count()}')
# Count and show sparse variables
df.select([count(when(isnan(c) | col(c).isNull() | (col(c) == "Unknown") | (col(c) == 0), c)).alias(c) for c in df.columns]).show()

# Convert the timestamps to the DateTime
ct_udf = udf(epoch_to_date, TimestampType())
df = df.withColumn('epochMillis',ct_udf(df['epochMillis']))
# Rename the epochMillis column
df = df.withColumnRenamed('epochMillis', 'date')
min_date, max_date = df.select(min('date'), max('date')).first()
print(f'Start date: {min_date}\nEnd date: {max_date}')
# Remove the time from the date
dt_udf = udf(date_to_ym, StringType())
df = df.withColumn('date',dt_udf(df['date'])).groupBy('date')
# Print the years with the months and the days for
# detecting the time intervals
df.count().orderBy('date').show()
