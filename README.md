## Deploy
Not nead to build, just use the next command in the Linux terminal:  
```spark-submit spark_explore.py```

## Answears
    1. Code in the file: spark_explore.py  
    2. I used Parquet dew to compact file size and it's easy to use in Spark.  
    3. Time periods:  
        - from 2019-03-23 to 2019-03-30
        - from 2020-03-21 to 2020-03-28  
    4. Top 3 sparse variables:  
        - cargoDetails  
        - imo  
        - callSign  
    5. Asia
